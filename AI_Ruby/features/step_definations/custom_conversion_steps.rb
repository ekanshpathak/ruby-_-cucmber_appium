Then("I press on create your first conversion button") do
  sleep 3
  # @eyes.check_window("Custom conversation")
  @driver.find_element(id:"btn_new_custom_conversion").click()
  sleep 3
end



And("I typed {string} as custom conversion name") do |name|
  sleep 4
  @driver.find_element(id:"edit_custom_conversion_category_name").send_keys(name)
  sleep 3
end



 When("I press on new unit button") do
   @driver.find_element(id:"btn_new_custom_unit").click()
   sleep 3
 end




Then("I typed {string} as unit name") do |unit_name|

  #wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds

  # element = wait.until { $driver.find_element(:id => "text_input_layout_unit_name") }

  name =  @driver.find_element(id:"text_input_layout_unit_name")
  sleep 10
  name.click()
  sleep 15
  @driver.action.send_keys(name,unit_name).perform
  sleep 10
    # name.send_keys("Horse Power")

end

Then("I typed {string} as unit symbol") do |unit_symbol|
  @driver.find_element(id:"edit_custom_conversion_unit_dtls_symbol").click()
  sleep 10
  @driver.find_element(id:"edit_custom_conversion_unit_dtls_symbol").send_keys(unit_symbol)
  sleep 10
end

Then("I typed {string} as unit value") do |unit_value|
  @driver.find_element(id:"edit_custom_conversion_unit_dtls_value").click()
  sleep 10
  @driver.find_element(id:"edit_custom_conversion_unit_dtls_value").send_keys(unit_value)
  # @eyes.check_window("Unit creation")
  sleep 10


end

And("I pressed submit check mark on Custom conversion screen") do
  @driver.find_element(id:"action_confirm_custom_unit").click()
  sleep 3
  # $driver.find_element(id:"btn_unit_dtls_ok").click()
  sleep 5
end


Then("I press on OK button on Custom conversion screen") do

  @driver.find_element(id:"btn_custom_conversion_details_ok").click()
  sleep 2
end



And("I verify {string} added to Custom conversions list") do |conversion_name|
  sleep 5
  # @eyes.check_window("Conversion Created")
  @driver.find_element(text:conversion_name)
end
